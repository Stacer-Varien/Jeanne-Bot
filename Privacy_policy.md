# Privacy Policy

This document entails the privacy policy and agreement that you accept when adding Jeanne (Bot ID 831993597166747679) to a server, or as a member of such a server. This document does not supersede the [Developer Terms of Service](https://discordapp.com/developers/docs/legal), [Discord's Terms of Service](https://discord.com/terms) and [Community Guidelines](https://discord.com/guidelines).

## Data Collection

Main data is stored in an SQLite Database. The data are backed up in case if there is need of change to prevent loss and damage and they can be accessed only by the bot's developer, [Varien-1936](https://github.com/Varien-1936). If an event of unintentional data loss or alteration, the developer will try to compensate to it to users who were affected to it.


User IDs, server IDs and channel IDs are the main data stored and kept privately in the database and secured but is not always 100% secured. The data is not sold or given to unauthorised 3rd parties unless required by law or Terms of Services. 

The user's or server's data is **not registered** in the database if:

1. The user has not send a message in the server or sending messages in ignored channels and direct messages between them and the bot or in a channel were the bot cannot view
2. A certian command required to add/enter data in the database has not been used at least once
3. If the user has been banned from using the bot

## Data Accessing

Data can be accessed but limited to anyone except the bot developer as he has full access to all kinds of data. Level and rank data can be accessed freely but data such as inventory can only be accessed by the main user. Sensitive data such as message logs are private or public depending on how authorised personnel see fit to it and will only be logged to their own servers.


## Removal of Data

Users are free to request a removal of data via joining the [support server](https://discord.gg/Vfa796yvNq) or email to zane544yt@protonmail.com contacting the bot developer with valid reasons.

## Questions

If you have any question concerning this policy, please feel free to join the server and ask or email to zane544yt@protonmail.com.

