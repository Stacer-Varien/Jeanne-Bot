Jeanne is a multipurpose bot with miscellaneous, moderation, management and some fun commands.

**FEATURES**

* Available slash commands
* Levelling system
* Moderation such as purge, warn, timeout, kick, ban and unban
* Create, delete and rename categories, roles and text and voice channels
* Fun commands you can play with
* Getting information of a user and server
* A say command that will make Jeanne say the message the user wants
* A reaction system such as slap, hug and pat
* A welcoming, leaving, message, member and modlogging system
* Advance embed generator


NOTE: THIS IS FOR EDUCATIONAL PURPORSES. ALSO I WOULD NOT RECOMMENDEND SELFHOSTING THE BOT AS THERE CAN BE CHANGES TO IT SUCH AS ADDING APIS THAT WILL MAKE YOU RUN ALMOST ALL FEATURES OF THE BOT

[Invite Jeanne](https://discord.com/api/oauth2/authorize?client_id=831993597166747679&permissions=2550197270&redirect_uri=https%3A%2F%2Fdiscord.com%2Foauth2%2Fauthorize%3Fclient_id%3D831993597166747679%26scope%3Dbot&scope=bot%20applications.commands)

<a href="https://top.gg/bot/831993597166747679">
  <img src="https://top.gg/api/widget/831993597166747679.svg" alt="Jeanne" />
  </a>
